package facci.chavezpaz.aplicaciondado

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


/**
 * Esta actividad permite al usuario tirar un dado y ver el resultado
 * en la pantalla.
 */
class MainActivity : AppCompatActivity() {
    class Dice(val numSides: Int) {

        fun roll(): Int {
            return (1..numSides).random()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.button)

        rollButton.setOnClickListener {
            val toast = Toast.makeText(this, "Dado lanzado!", Toast.LENGTH_SHORT)
            toast.show()
            rollDice()
        }

        rollDice()
    }

    /**
     * Tira los dados y actualiza la pantalla con el resultado.
     */
    private fun rollDice() {

        // Crea un nuevo objeto Dice con 6 lados y hazlo rodar
        val dice = Dice(6)
        val diceRoll = dice.roll()
        // Actualizar la pantalla con la tirada de dados
        val diceImage: ImageView = findViewById(R.id.imageView)

        val drawableResource = when (diceRoll) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }

        //Actualice ImageView con el ID de recurso dibujable correcto
        diceImage.setImageResource(drawableResource)

        // Actualizar la descripción del contenido
        diceImage.contentDescription = diceRoll.toString()
    }


}